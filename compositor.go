package gg

import (
	"image"
	"image/color"
	"image/draw"
)

func NewCompositor(canvas draw.Image) *Compositor {
	return &Compositor{canvas: canvas}
}

type Compositor struct {
	canvas draw.Image
	layers []*CompositorLayer
}

type CompositorLayer struct {
	compositor *Compositor
	at         image.Point
	bounds     image.Rectangle
	image      image.Image
}

func (c *Compositor) Bounds() image.Rectangle {
	return c.canvas.Bounds()
}

func (c *Compositor) AddLayer() *CompositorLayer {
	l := new(CompositorLayer)
	l.compositor = c
	c.layers = append(c.layers, l)
	return l
}

func (l *CompositorLayer) Draw(image image.Image) {
	l.DrawAt(image, l.at)
}

func (l *CompositorLayer) DrawAt(image image.Image, at image.Point) {
	oldBounds := l.bounds

	l.at = at
	l.image = image
	l.bounds = image.Bounds().Add(l.at)

	// If the new bounds do not completely contain the old bounds, redraw the
	// old bounds
	if !oldBounds.In(l.bounds) {
		l.compositor.Redraw(oldBounds)
	}

	l.compositor.Redraw(l.bounds)
}

func (c *Compositor) Redraw(r image.Rectangle) {
	r = c.canvas.Bounds().Intersect(r)
	m := image.NewRGBA(r)
	for _, l := range c.layers {
		r := l.bounds.Intersect(r)
		for y := r.Min.Y; y < r.Max.Y; y++ {
			for x := r.Min.X; x < r.Max.X; x++ {
				r1, g1, b1, a1 := m.At(x, y).RGBA()
				r2, g2, b2, a2 := l.image.At(x-l.at.X, y-l.at.Y).RGBA()
				r := uint16((r2*a2 + r1*(0xFFFF-a2)) / 0xFFFF)
				g := uint16((g2*a2 + g1*(0xFFFF-a2)) / 0xFFFF)
				b := uint16((b2*a2 + b1*(0xFFFF-a2)) / 0xFFFF)
				a := uint16(0xFFFF - (0xFFFF-a2)*(0xFFFF-a1)/0xFFFF)
				m.SetRGBA64(x, y, color.RGBA64{r, g, b, a})
			}
		}
	}

	for y := r.Min.Y; y < r.Max.Y; y++ {
		for x := r.Min.X; x < r.Max.X; x++ {
			c.canvas.Set(x, y, m.At(x, y))
		}
	}
}
