package gg

import (
	"image"
	"image/draw"

	"github.com/golang/freetype/raster"
)

type Canvas interface {
	draw.Image
	PainterFor(mask *image.Alpha, pattern image.Image) raster.Painter
}

type imageCanvas struct {
	draw.Image
}

func (c *imageCanvas) PainterFor(mask *image.Alpha, pattern image.Image) raster.Painter {
	return &patternPainter{c.Image, mask, pattern}
}

type rgbaCanvas struct {
	*image.RGBA
}

func (c *rgbaCanvas) PainterFor(mask *image.Alpha, pattern Pattern) raster.Painter {
	if pattern, ok := pattern.(*solidPattern); ok && mask == nil {
		// with a nil mask and a solid color pattern, we can be more efficient
		// TODO: refactor so we don't have to do this type assertion stuff?
		p := raster.NewRGBAPainter(c.RGBA)
		p.SetColor(pattern.color)
		return p
	}

	return &rgbaPatternPainter{c.RGBA, mask, pattern}
}
